﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 7;
    public float jumpPower = 15;
    public float speedAttack = 25;
    public Transform BottomPlayer;
    public LayerMask groundMask;
    public GameObject Skull;
    public ParticleSystem explosionParticle;
    public Sound PlayerDied;

    private Rigidbody2D rb;
    private float playerRadius = 0.07f;
    private bool isRight = true;
    private bool onGround = true;
    private Transform skullTransform;
    private Rigidbody2D skullRigidbody;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        skullTransform = Skull.GetComponent<Transform>();
        skullRigidbody = Skull.GetComponent<Rigidbody2D>();

        PlayerPrefs.SetInt("PlayerIsJump", 0);
        PlayerPrefs.SetInt("PlayerIsGround", 1);
        PlayerPrefs.SetInt("PlayerIsAttack", 0);
    }

    private void FixedUpdate()
    {
        if (PlayerPrefs.GetInt("TurnPlayer") == 1)
        {
            if (PlayerPrefs.GetInt("TimeGame") == 5)
            {
                Skull.SetActive(true);
            }

            onGround = Physics2D.OverlapCircle(BottomPlayer.position, playerRadius, groundMask);

            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                if (isRight)
                {
                    isRight = false;
                    rb.transform.Rotate(0, 180, 0);
                }

                Move();
            }

            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                if (!isRight)
                {
                    isRight = true;
                    rb.transform.Rotate(0, 180, 0);
                }

                Move();
            }

            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                Skull.transform.Rotate(0, 0, skullTransform.rotation.z + 1);
            }

            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                Skull.transform.Rotate(0, 0, skullTransform.rotation.z - 1);
            }

            if (Input.GetKey(KeyCode.Mouse0))
            {
                Attack();
                Invoke(nameof(StopAttack), 2);
            }

            if (Input.GetKey(KeyCode.Space) && onGround)
            {
                Jump();
            }

            if (onGround)
            {
                PlayerPrefs.SetInt("PlayerIsGround", 1);
            }
            else
            {
                PlayerPrefs.SetInt("PlayerIsGround", 0);
            }

            PlayerPrefs.SetFloat("PlayerVelocity", Input.GetAxis("Horizontal"));
        } 
    }

    private void Move()
    {
        Vector3 movement = new Vector3(Input.GetAxis("Horizontal"), 0f, 0f);
        rb.transform.position += movement * Time.deltaTime * moveSpeed;
    }

    private void Jump()
    {
        PlayerPrefs.SetInt("PlayerIsGround", 0);
        PlayerPrefs.SetInt("PlayerIsJump", 1);
        rb.velocity = new Vector2(rb.velocity.x, jumpPower);
    }

    private void Attack()
    {
        PlayerPrefs.SetInt("PlayerIsAttack", 1);
        skullRigidbody.velocity = skullTransform.right.normalized * speedAttack;
        skullRigidbody.gravityScale = 5;
    }

    private void StopAttack()
    {
        PlayerPrefs.SetInt("PlayerIsAttack", 0);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "SkullEnemy")
        {
            gameObject.transform.localScale = new Vector3(0, 0, 0);
            explosionParticle.Play();
            gameObject.transform.localScale = new Vector3(0, 0, 0);
            Skull.SetActive(false);

            PlayerPrefs.SetString("Winner", "ENEMY WINS");
            AudioManager.Instance.PlaySound(PlayerDied);
            Invoke(nameof(GameOver), 2);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "DeathZone")
        {
            PlayerPrefs.SetString("Winner", "ENEMY WINS");
            AudioManager.Instance.PlaySound(PlayerDied);
            Invoke(nameof(GameOver), 2);
        }
    }

    private void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
