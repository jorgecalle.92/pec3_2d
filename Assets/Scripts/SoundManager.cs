﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public Sound mainSound;

    void Start()
    {
        AudioManager.Instance.PlaySound(mainSound);
    }
}
