﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public Text textWinner;

    void Start()
    {
        textWinner.text= PlayerPrefs.GetString("Winner");
        Invoke(nameof(MainMenu), 3);
    }

    private void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }
}
