﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullManager : MonoBehaviour
{
    private Vector3 originalPosition;
    private Vector3 originalRotation;

    private void Awake()
    {
        originalPosition= new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z);
    }

    private void FixedUpdate()
    {
        if(PlayerPrefs.GetInt("TimeGame") < 1)
        {
            resetSkull();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Block")
        {
            resetSkull();
        }
    }

    private void resetSkull()
    {
        this.GetComponent<Rigidbody2D>().velocity = this.transform.right.normalized * 0;
        this.GetComponent<Rigidbody2D>().gravityScale = 0;
        //Reset position and rotation
        this.transform.localPosition = new Vector3(-0.198f, 0.046f, 0);
        this.transform.rotation = Quaternion.identity;
        this.gameObject.SetActive(false);
    }
}
