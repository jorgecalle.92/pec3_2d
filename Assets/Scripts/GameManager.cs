﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text textTime;
    public Text textTurn;
    public int time = 5;
    public Sound LevelSound;

    private void Awake()
    {
        AudioManager.Instance.PlaySound(LevelSound);
    }

    private void Start()
    {
        PlayerPrefs.SetInt("TimeGame", time);
        PlayerPrefs.SetInt("TurnPlayer", 1);

        InvokeRepeating(nameof(SusbstractTime), 1f, 1f);
    }
    
    private void Update()
    {
        PlayerPrefs.SetInt("TimeGame", time);
        
        textTime.text = time.ToString();
    }

    private void SusbstractTime()
    {
        if (time > 0)
        {
            time--;
        }
        else
        {
            time = 5;

            if(PlayerPrefs.GetInt("TurnPlayer") == 1)
            {
                PlayerPrefs.SetInt("TurnPlayer", 0);
                textTurn.text = "Enemy";
            }
            else
            {
                PlayerPrefs.SetInt("TurnPlayer", 1);
                textTurn.text = "Player";
            }
        }
    }

}
