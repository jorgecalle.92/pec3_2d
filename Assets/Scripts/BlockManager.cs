﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockManager : MonoBehaviour
{
    public ParticleSystem explosionParticle;
    public Sound Explosion;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "SkullPlayer" || collision.gameObject.tag == "SkullEnemy")
        {
            if (!AudioManager.Instance.IsFXPlaying())
            {
                AudioManager.Instance.PlaySound(Explosion);
            }

            gameObject.transform.localScale = new Vector3(0,0,0);
            explosionParticle.Play();
            Destroy(gameObject, explosionParticle.duration);
        }
    }
}