﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEnemy : MonoBehaviour
{
    private Rigidbody2D rigidbodyEnemy;
    private Animator animatorEnemy;

    void Awake()
    {
        rigidbodyEnemy = GetComponent<Rigidbody2D>();
        animatorEnemy = GetComponent<Animator>();
    }

    void Update()
    {
        if (PlayerPrefs.GetInt("EnemyIsJump") == 1 && PlayerPrefs.GetInt("EnemyIsGround") == 0)
        {
            animatorEnemy.SetBool("EnemyIsJump", true);
            animatorEnemy.SetBool("EnemyIsGround", false);
        }
        else
        {
            animatorEnemy.SetBool("EnemyIsJump", false);
            animatorEnemy.SetBool("EnemyIsGround", true);
        }

        if (PlayerPrefs.GetInt("EnemyIsAttack") == 1)
        {
            animatorEnemy.SetBool("EnemyIsAttack", true);
        }
        else
        {
            animatorEnemy.SetBool("EnemyIsAttack", false);
        }
    }
}
