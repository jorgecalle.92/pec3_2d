﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed = 7;
    public float jumpPower = 15;
    public float speedAttack = 25;
    public Transform BottomEnemy;
    public LayerMask groundMask;
    public GameObject Skull;
    public ParticleSystem explosionParticle;
    public Sound EnemyDied;

    private Rigidbody2D rb;
    private float playerRadius = 0.07f;
    private bool onGround = true;
    private bool stopMove = false;
    private Transform skullTransform;
    private Rigidbody2D skullRigidbody;
    private bool isRotate= false;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        skullTransform = Skull.GetComponent<Transform>();
        skullRigidbody = Skull.GetComponent<Rigidbody2D>();

        PlayerPrefs.SetInt("EnemyIsJump", 0);
        PlayerPrefs.SetInt("EnemyIsGround", 1);
        PlayerPrefs.SetInt("EnemyIsAttack", 0);
    }

    private void FixedUpdate()
    {
        if(PlayerPrefs.GetInt("TurnPlayer") == 0)
        {
            onGround = Physics2D.OverlapCircle(BottomEnemy.position, playerRadius, groundMask);

            if (!stopMove)
            {
                Move();
                Invoke(nameof(StopMove), 0.5f);
            }
        }
        else
        {
            stopMove = false;
        }
    }

    private void Move()
    {
        Vector3 movement = new Vector3(-0.2f, 0f, 0f);
        rb.transform.position += movement * Time.deltaTime * moveSpeed;

        if (onGround)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpPower);
        }

        PlayerPrefs.SetInt("EnemyIsGround", 0);
        PlayerPrefs.SetInt("EnemyIsJump", 1);
    }

    private void StopMove()
    {
        stopMove = true;
        PlayerPrefs.SetInt("EnemyIsGround", 1);
        PlayerPrefs.SetInt("EnemyIsJump", 0);
        
        Skull.SetActive(true);

        if(!isRotate)
        {
            Skull.transform.Rotate(0, 0, -Random.Range(45, 90));
            isRotate = true;
        }

        Invoke(nameof(Attack), 1);
    }

    private void Attack()
    {
        PlayerPrefs.SetInt("EnemyIsAttack", 1);
        
        skullRigidbody.velocity = -skullTransform.right.normalized * speedAttack;
        skullRigidbody.gravityScale = 5;

        Invoke(nameof(StopAttack), 2);
    }

    private void StopAttack()
    {
        PlayerPrefs.SetInt("EnemyIsAttack", 0);
        isRotate = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "SkullPlayer")
        {
            gameObject.transform.localScale = new Vector3(0, 0, 0);
            explosionParticle.Play();
            gameObject.transform.localScale = new Vector3(0, 0, 0);
            Skull.SetActive(false);

            PlayerPrefs.SetString("Winner", "PLAYER WINS");
            AudioManager.Instance.PlaySound(EnemyDied);
            Invoke(nameof(GameOver), 2);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.tag == "DeathZone")
        {
            PlayerPrefs.SetString("Winner", "PLAYER WINS");
            AudioManager.Instance.PlaySound(EnemyDied);
            Invoke(nameof(GameOver), 2);
        }
    }

    private void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }
}
