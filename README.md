# PEC3

En este proyecto se ha realizado un juego de artillería al estilo de los videojuegos Worms o Gorillas.
Hay dos personajes, uno controlado por el jugador y otro controlado por la máquina. El objetivo del juego, es derrotar al otro lanzando una calavera.

Para ello, cada personaje dispone de un turno de cinco segundos, en el que se puede mover, saltar y disparar.
El juego dispone de tres escenas, cada una con su propia música: MainMenu (el menú principal), Level (el nivel del juego) y GameOver (pantalla de fin de la partida):

*  La escena de **MainMenu** contiene dos botones, uno para iniciar la partida y otro para salir del juego. Contiene los siguientes componentes:

   1. **AudioManager:** mediante los scripts Sound y AudioManager se realiza un control de toda la música y los efectos de sonido del juego, por lo que se guarda como un Prefab y se reutiliza en las otras dos escenas.

   2. **SoundManager:** contiene el script SoundManager con el que se inicia la música principal de la escena.

   3. **GameSceneManager:** se hace uso del script GameSceneManager, con el cual se permite realizar el cambio entre escenas. Se guarda como un prefab y se utiliza en las otras dos escenas del juego.


*  La escena de **Level** es la que contiene todo el nivel del juego. Además de un escenario compuesto por bloques los cuales se pueden destruir, el nivel contiene tres águilas que se están movimiento constantemente para entorpecer los disparos de los personajes.
   Cada personaje dispone de cinco segundos para realizar distintas acciones, como moverse, saltar o disparar. El juego finaliza cuando uno de los dos personajes es derrotado.
 
   Contiene los siguientes componentes:

   1. **Stage:** en el stage se encuentran tanto los bloques de los que se compone el nivel, como el fondo del juego y unas águilas que se mueven constantemente. Las águilas solo tienen la función de molestar a los personajes, para que reboten los disparos en ellas. En cuanto a los bloques, mediante el script BlockManager se controla el momento en el que recibe un disparo, para que de esta forma salte una animación de partículas simulando una explosión y posteriormente se destruirá el bloque.

   2. **AudioManager:** tiene la misma función que en la escena anterior, realiza un control de la música y los efectos de sonido del juego.

   3. **GameManager:** el script GameManager es el que se encarga de ejecutar la música principal del juego, a la vez que controla la interfaz con el tiempo restante del personaje y si es el turno del jugador o del enemigo.
   
   4. **Player:** mediante el script PlayerController se controlan las acciones del personaje que controla el jugador con sus distintas animaciones: moverse, saltar y atacar. Para poder atacar, el jugador tiene la posibilidad de rotar la calavera con las flechas de arriba y abajo, para determinar la dirección en la que será disparada. También se realiza el control de cuando el personaje es golpeado por la calavera del enemigo.
   
   5. **Enemy:** el scipt EnemyController, además de detectar cuando es golpeado por el jugador, controla las acciones del enemigo. En este caso, se ha diseñado un movimiento predefinido, en la que el personaje se mueve y salta al comenzar su turno. Después de saltar, el personaje realiza un disparo aplicando una rotación aleatoria.
   
   6. **DeathZone:** este componente es el que comprueba si uno de los personajes cae al vacío, en cuyo momento, perderá la partida. 
   

*  Por último, la escena **GameOver** es la que aparece cuando uno de los personajes es derrotado. La escena dura 3 segundos antes de regresar a la pantalla del menú principal. En esta escena, además de sonar una música y mostrar un texto que indican el fin de la partida, aparece un texto en el que se muestra el ganador del juego. Contiene los siguientes componentes:

   1. **GameOverManager:**  el script GameOverManager es el que se encarga de modificar el texto que indica el ganador de la partida.
   
   2. **AudioManager:** tiene la misma función que en las escenas anteriores, realiza un control de la música del juego.
   
   3. **SoundManager:** contiene el script SoundManager con el que se inicia la música principal de la escena.
